export interface LoggedUser {
  email?: string;
  jwt?: string;
}
