import { Captcha } from "./captcha";

export interface CaptchaRegisterRequest {
  email: string,
  password: string,
  captcha: Captcha
}
