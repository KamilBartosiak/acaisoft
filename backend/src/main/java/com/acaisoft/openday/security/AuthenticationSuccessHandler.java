package com.acaisoft.openday.security;


import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class AuthenticationSuccessHandler {



    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, String jwtToken) {

        response.setStatus(HttpServletResponse.SC_OK);
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        response.setHeader(JwtAuthenticationFilter.JWT_HEADER_TOKEN_NAME, jwtToken);

    }
}
