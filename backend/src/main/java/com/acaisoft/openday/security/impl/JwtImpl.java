package com.acaisoft.openday.security.impl;


import com.acaisoft.openday.AppException;
import com.acaisoft.openday.security.Jwt;
import com.acaisoft.openday.security.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class JwtImpl implements Jwt{
    private final String jwtKey;

    public JwtImpl(String jwtKey) {
        this.jwtKey = jwtKey;
    }

    @Override
    public JwtUser jwtUser(String text) throws Exception {
        Claims claims = parse(text, this.jwtKey);

        String username = claims.get(JwtUser.CLAIMS_PARAM_NAMES.USER_NAME, String.class);
        Collection<SimpleGrantedAuthority > authorities = authorities(listText(claims, JwtUser.CLAIMS_PARAM_NAMES.ROLES));

        return new JwtUserImpl(this.jwtKey,
                new UserDetailsByUsername(username, authorities)
            );
    }

    @Override
    public String token(UserDetails userDetails) throws Exception {
        JwtUser jwtUser = new JwtUserImpl(this.jwtKey, userDetails);
        return jwtUser.token();
    }


    private Claims parse(String claimsJws, String signingKey) throws Exception{
        try {
            return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(claimsJws).getBody();
        } catch (Exception ex){
            throw new AppException("Failed to parse jwt ", ex);
        }
    }

    private List<SimpleGrantedAuthority> authorities(List<String> roles){
        return roles
            .stream()
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());
    }

    public List<String> listText(Claims claims, String key){
        Object obj = claims.get(key);

        List<?> list = ArrayList.class.cast(obj);

        List<String> results = new ArrayList<>();
        for(Object ele : list){
            results.add(ele.toString());
        }

        return results;
    }
}
