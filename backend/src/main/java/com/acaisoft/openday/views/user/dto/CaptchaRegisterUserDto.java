package com.acaisoft.openday.views.user.dto;

import javax.validation.constraints.NotNull;

public class CaptchaRegisterUserDto {
    @NotNull
    private String email;
    @NotNull
    private String password;
    @NotNull
    private CaptchaDto captcha;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public CaptchaDto getCaptcha() {
        return captcha;
    }
}
