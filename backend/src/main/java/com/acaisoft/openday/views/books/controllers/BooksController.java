package com.acaisoft.openday.views.books.controllers;

import com.acaisoft.openday.db.BookslistDB;
import com.acaisoft.openday.db.BookslistDBException;
import com.acaisoft.openday.schema.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BooksController {

    private BookslistDB bookslistDB;

    @Autowired
    public BooksController(BookslistDB bookslistDB) {
        this.bookslistDB = bookslistDB;
    }

    @GetMapping("/bookslist")
    public ResponseEntity<List<Book>> getBooksList() {
        return ResponseEntity.ok(this.bookslistDB.getBooksList());
    }

    @RequestMapping(value = "/bookslist/{bookId}/borrow", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Book> borrowBook(@PathVariable Long bookId) throws Exception {

        Book book = null;
        for (Book b : bookslistDB.getBooksList()) {
            if (Long.parseLong(b.getId()) == bookId) {
                book = b;
            }
        }
        if (book != null) {
            if (book.getQuantity() > 0) {
                book.setQuantity(book.getQuantity() - 1);
                return ResponseEntity.ok(book);
            } else {
                throw new BookslistDBException("Not enought books");
            }
        } else {
            throw new BookslistDBException("Book does not exists");
        }

    }

}
